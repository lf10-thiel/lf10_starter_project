import { Component} from '@angular/core';
import {Employee} from "../Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-display-db',
  templateUrl: './display-db.component.html',
  styleUrls: ['./display-db.component.css']
})

export class DisplayDBComponent {
  bearer = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDU2MzI3NDIsImlhdCI6MTY0NTYxODM0MiwianRpIjoiYjMwNTFiYjktNzQ2Yi00NTM3LTg3MTktMzlhMmNlYWExYjViIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiI0NjA3OWEyMi1kOGVhLTRkYmUtODNjYi0zOGI0M2QyMjAwNTgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.W0V0MMoZYyMT6QwO1MopqLbEg4m6V9gPbcL3hPiT6yyeXP9-2OxUgmD2iQWGbkCac2erJNr3XEo93xQt4NuKIFsKtFriZfP4jK5b9yrBbY5dGVwfsq_mXuSsmz40qf3ob2JAtRz-LgzyxZxkEJW66aEdUlApv9Yt5FZ4ZTuAs5SFUCK0ZSqJBL5onzWIfc3h7wp2q02biJ56DPmgUWHnFqthTIx3eZTGvlrI2iiWlcFjdk-SiVSjCMWNgI_y1SnRq2Ue-WGf0UyhXcxShwP0T2QfvUTGbJ2L0x3aaLobtNwiAtaKQHV-VC-yK58Jnv809udc6DNIbOIo-QLOnUIfSg';
  employees$: Observable<Employee[]>;

  constructor(private http: HttpClient) {
    this.employees$ = of([]);
    this.fetchData();
  }

  fetchData() {
    this.employees$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    });
  }


  goToNewEmployee() {
    window.open("app-create-new-employee","_self");
  }
}
