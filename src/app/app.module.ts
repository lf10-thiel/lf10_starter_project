import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { DisplayDBComponent } from './DatabaseMainWindow/display-db.component';
import { CreateNewEmployeeComponent } from './create-new-employee/create-new-employee.component';

@NgModule({
  declarations: [
    AppComponent,
    DisplayDBComponent,
    CreateNewEmployeeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
